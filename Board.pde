class Board {
  // Constants
  static final int SPACING = 12, RECT_SIZE = 70, CURVE = 3;
  static final int BOARD_COLOR = 153, EMPTY_TILE_COLOR = 200;
  static final int STARTX = 50, STARTY = 150;
  static final int UP = 1, DOWN = 2, LEFT = 3, RIGHT = 4;
  
  // Attributes
  ArrayList<Tile> tiles = new ArrayList<Tile>();
  int squareArraySize;
  int maxX, maxY;
  int numPoints;

  // Getters & Setters
  void addTile(Tile tile) {
    tiles.add(tile);
  }
  void removeTile(Tile tile) {
    tiles.remove(tile);
  }
  ArrayList<Tile> getTiles() {
    return tiles; 
  }
  Tile getTileAt(int row, int col) {
     for (Tile tile : tiles) {
       if (tile.getRow() == row && tile.getCol() == col) 
         return tile;
     }
     return new Tile(row, col, new Board(4));
  }
  int getSquareArraySize() {
    return squareArraySize;
  }
  int getNumPoints() {
    return numPoints;
  }
  
  // Constructor
  Board(int squareArraySize) {
    // Make coordinates for two random tiles
    int row1, col1, row2, col2;
    do {
      row1 = (int) random(squareArraySize);
      col1 = (int) random(squareArraySize);
      row2 = (int) random(squareArraySize);
      col2 = (int) random(squareArraySize);
    } while (row1 == row2 && col1 == col2);
    
    // Add the new tiles
    Tile tile1 = new Tile(row1, col1, this), tile2 = new Tile(row2, col2, this);
    this.squareArraySize = squareArraySize;
    maxX = STARTX + squareArraySize * (RECT_SIZE + SPACING);
    maxY = STARTY + squareArraySize * (RECT_SIZE + SPACING);
  }
  
  // Draw Function
  void draw() {
    // Draw the background square for the board
    fill(BOARD_COLOR);
    stroke(BOARD_COLOR);
    rect(STARTX - 12, STARTY - 12, maxX - STARTX + 10, maxY - STARTY + 10, CURVE);

    // Draw the empty spaces on the board
    stroke(EMPTY_TILE_COLOR);
    fill(EMPTY_TILE_COLOR);
    for (int x = STARTX; x < maxX; x += RECT_SIZE + SPACING) {
      for (int y = STARTY; y < maxY; y += RECT_SIZE + SPACING) {
        rect(x, y, RECT_SIZE, RECT_SIZE, CURVE);
      }
    }
    
    // Draw all tiles
    for (Tile tile : tiles) {
      tile.draw(); 
    }
  }
  
  // Methods
  boolean hasTileAt(int row, int col) {
    for (Tile tile : tiles) {
      if (tile.getRow() == row && tile.getCol() == col) 
        return true;
    }
    return false;
  }
  
// ----------------------------------------------------------------------
//  MOVE TILES
// ----------------------------------------------------------------------
  
  void moveTiles(int dir) {
    // Keep track of if any tiles moved
    boolean moved = false;
    
    // Set row difference
    int rowDir = 0, colDir = 0;
    if (dir == UP)
      rowDir = -1;
    else if (dir == DOWN)
      rowDir = 1;
    else if (dir == LEFT)
      colDir = -1;
    else if (dir == RIGHT)
      colDir = 1;
    
    // Go thru tiles (in a particular order) 
    for (int i = 0; i < squareArraySize; i++) {
      for (int j = 0; j < squareArraySize; j++) {
        // Set row and column
        int row = i, col = j;
        if (dir == DOWN) {
          row = squareArraySize - 1 - i;
        } else if (dir == LEFT) {
          row = j; 
          col = i;
        } else if (dir == RIGHT) {
          row = j;
          col = squareArraySize - 1 - i;
        }
        
        // If there's a tile, move the tile & try to combine
        if (hasTileAt(row, col)) {
          Tile tile = getTileAt(row, col);
          // Move the tile in that direction as far as it can go
          for (int k = 0; k < 3; k++) {
            int newRow = row + rowDir, newCol = col + colDir;
            if (newRow > -1 && newRow < squareArraySize && newCol > -1 && newCol < squareArraySize && !hasTileAt(newRow, newCol)) {
                tile.setRow(newRow);
                tile.setCol(newCol);
                row = newRow;
                col = newCol;
                moved = true;
            } else
                break;
          }
          // Look at the tile after the tile and see if it can combine
          int newRow = row + rowDir, newCol = col + colDir;
          if (hasTileAt(newRow, newCol) && getTileAt(newRow, newCol).getNum() == tile.getNum()) {
            combineTiles(tile, getTileAt(newRow, newCol));
            moved = true;
          }
        }
      }
    }
    if (moved) {
      int x, y;
      do {
        x = (int) random(squareArraySize);
        y = (int) random(squareArraySize);
      } while (hasTileAt(x, y));
      Tile newTile = new Tile(x, y, this);
    }
    
  }
  
  void combineTiles(Tile tile1, Tile tile2) {
    int newRow = tile2.getRow(), newCol = tile2.getCol();
    int num = tile1.getNum();
    removeTile(tile1);
    removeTile(tile2);
    new Tile(num * 2, newRow, newCol, this);
    numPoints += num * 2;
  }
  
// ----------------------------------------------------------------------
//  END GAME
// ----------------------------------------------------------------------
  
  // Checks if any tiles on the board can combine
  boolean canTilesCombine() {
    // Loop through all tiles on the board
    for (Tile tile : tiles) {
      // Look at each of these pairs of indices
      int row = tile.getRow(), col = tile.getCol();
      int[][] indices = { {-1, 0}, {1, 0}, {0, -1}, {0, 1} };
      // See if a tile at any of those indices (compared to the orig tile) could combine with it
      for (int[] index : indices) {
        int newRow = row + index[0], newCol = col + index[1];
        // If the indices work and there's a tile at that spot and it's not the orig spot,
          if (newRow > -1 && newRow < squareArraySize &&
              newCol > -1 && newCol < squareArraySize && 
              !(newRow == row && newCol == col) && hasTileAt(newRow, newCol)) {
            Tile newTile = getTileAt(newRow, newCol);
            // ... and the tiles have the same number, then they could be combined
            if (newTile.getNum() == tile.getNum())
              return true;
           }
       }
    }
    return false;
  }
  
  boolean isGameOver() {
    return (tiles.size() == squareArraySize * squareArraySize) && !canTilesCombine();
  }
  
  
  
}
