Board board = new Board(4);

void setup() {
  // Set size and background color of frame
  size(550, 550);
  background(240);
  noFill();
  
  // Write points number at top
  fill(0);
  textSize(20);
  textAlign(LEFT);
  text("Number of Points:", 42, 75); 
  text("0", 225, 75);
}

void draw() {
  fill(240);
  stroke(240);
  rect(220, 50, 75, 35);
  
  fill(0);
  textAlign(LEFT);
  textSize(20);
  text(str(board.getNumPoints()), 225, 75);
  
  board.draw();
  if (board.isGameOver()) {
    fill(0);
    textSize(20);
    textAlign(LEFT);
    text("GAME OVER", 42, 110); 
  }
  
  
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      board.moveTiles(1);
    } else if (keyCode == DOWN) {
      board.moveTiles(2);
    } else if (keyCode == LEFT) {
      board.moveTiles(3);
    } else if (keyCode == RIGHT) {
      board.moveTiles(4);
    }
  }
  
}
