class Tile {
  // Constants
  static final int STARTX = 50, STARTY = 150, SPACING = 12, RECT_SIZE = 70, CURVE = 3;
  
  // Attributes
  int num;
  float oldRow, oldCol;
  int row, col;
  boolean justCombined;
  Board board;
  
  // Constructors
  Tile(int num, int row, int col, Board board) {
    this.num = num;
    this.row = row;
    this.oldRow = row;
    this.col = col;
    this.oldCol = col;
    this.justCombined = true;
    this.board = board;
    board.addTile(this);
  }
  Tile(int row, int col, Board board) {
    this(0, row, col, board);
    // Generate number to go on the tile: 
    float prob = random(10);
    if (prob > 7)
      this.num = 4;
    else 
      this.num = 2;
  }
  
  // Getters & Setters
  int getNum() {
    return num;
  }
  int getRow() {
    return row; 
  }
  void setRow(int row) {
    this.row = row;
  }
  int getCol() {
    return col;
  }
  void setCol(int col) {
    this.col = col;
  }
  Board getBoard() {
    return board;
  }
  
  int[] getColor(int num) {
    switch (num) {
      case 2 : {
        int[] colorArray = {245, 237, 225};
        return colorArray;
      }
      case 4 : {
        int[] colorArray = {240, 222, 196};
        return colorArray;
      }
      case 8 : {
        int[] colorArray = {245, 184, 91};
        return colorArray;
      } 
      case 16 : {
        int[] colorArray = {247, 161, 30};
        return colorArray;
      }
      case 32 : {
        int[] colorArray = {216, 122, 74};
        return colorArray;
      }
      case 64 : {
        int[] colorArray = {211, 76, 9};
        return colorArray;
      }
      default : {
        int[] colorArray = {250, 220, 71};
        return colorArray;
      }
    }
  }
  
  // Draw Function
  void draw() {
    if (justCombined) {
      
      float currSize = (float) RECT_SIZE + 1;
      do {
        // Location to draw the tile
        float x = STARTX + oldCol * (currSize + SPACING);
        float y = STARTY + oldRow * (currSize + SPACING);
   
        // Draw tile
        int[] colorArray = getColor(num);
        stroke(colorArray[0], colorArray[1], colorArray[2]);
        fill(colorArray[0], colorArray[1], colorArray[2]);
        rect(x, y, currSize, currSize, CURVE);
    
        // Draw number
        fill(0);
        if (num < 1000)
          textSize(25);
        else if (num < 10000)
          textSize(23);
        else 
          textSize(20);
      
        textAlign(CENTER);
        text(str(num), x + currSize / 2, y + currSize / 2 + 9); 
        
        currSize -= .005;
      } while (currSize > (float) RECT_SIZE);
      justCombined = false;
    }
    
    do {
      // Location to draw the tile
      float x = STARTX + oldCol * (RECT_SIZE + SPACING);
      float y = STARTY + oldRow * (RECT_SIZE + SPACING);
   
      // Draw tile
    
      int[] colorArray = getColor(num);
      stroke(colorArray[0], colorArray[1], colorArray[2]);
      fill(colorArray[0], colorArray[1], colorArray[2]);
      rect(x, y, RECT_SIZE, RECT_SIZE, CURVE);
    
      // Draw number
      fill(0);
      if (num < 1000)
        textSize(25);
      else if (num < 10000)
        textSize(23);
      else 
        textSize(20);
      
      textAlign(CENTER);
      text(str(num), x + RECT_SIZE / 2, y + RECT_SIZE / 2 + 9); 
      
      
      // Update the new row and column
      if (oldRow != row) {
        if (oldRow > row)
          oldRow -= .5;
        else
          oldRow += .5;
      } else if (oldCol != col) {
        if (oldCol > col) 
          oldCol -= .5;
        else
          oldCol += .5;
      }
      
    } while (oldRow != row && oldCol != col);
  }
  
  
}
