## A recreation of the popular game 2048 using Processing

My goal for this project was to recreate 2048 and to learn how to use 
Processing. The object of game is to use the arrow keys to combine tiles of the 
same value and create new tiles of a higher value. A player tries to get the highest 
value tile possible - once you've gotten the 2048 tile, you've won, but can continue 
playing to get even higher number tiles. To play, follow these steps:

1. Download Processing if needed [here](https://processing.org/download/)
2. Download all .pde files and save in a folder called Processing2048
3. Open processing2048.pde in Processing and hit run